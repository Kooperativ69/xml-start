package at.spenger.xml.shiporder;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.format.DateTimeFormatterBuilder;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;

public class Product {
	
	
	@XmlElement(required = true)
    protected String title;
	
	
    @Override
	public String toString() {
		return "Product [title=" + title + ", from=" + getFromDate() + ", to="
				+ getToDate() + ", price=" + price + "]";
	}
    
    @XmlElement(required = true)
    protected float price;
    
    @XmlElement(required = true)
	private LocalDate dateFrom;
    
    @XmlElement(required = true)
	protected LocalDate dateTo;

    /**
     * Ruft den Wert der title-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Legt den Wert der title-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Ruft den Wert der titel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFromDate() {
    	if (dateFrom != null)
    		return dateFrom.getDayOfMonth() + "."+dateFrom.getMonthValue() +"."+dateFrom.getYear();
    	return null;
    }

    /**
     * Legt den Wert der datefrom-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFromDate(LocalDate value) {
        this.dateFrom = value;
    }

    /**
     * Ruft den Wert der datefrom-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Date }
     *     
     */
    public String getToDate() {
    	if (dateFrom != null)
    		return dateTo.getDayOfMonth() + "."+dateTo.getMonthValue() +"."+dateTo.getYear();
    	return null;
    }

    /**
     * Legt den Wert der dateto-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToDate(LocalDate value) {
        this.dateTo = value;
    }

    /**
     * Ruft den Wert der dateto-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Date }
     *     
     */
    public float getPrice() {
        return price;
    }

    /**
     * Legt den Wert der price-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setPrice(float value) {
        this.price = value;
    }

}

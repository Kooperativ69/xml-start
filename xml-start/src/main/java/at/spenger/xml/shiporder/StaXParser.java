package at.spenger.xml.shiporder;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.XMLEvent;

public class StaXParser {

	public List<Product> read(InputStream in) throws IOException {
		final String PRODUCT = "product";
		List<Product> products = new ArrayList<>();

		// First, create a new XMLInputFactory
		XMLInputFactory inputFactory = XMLInputFactory.newInstance();

		// Setup a new eventReader
		try {
			XMLEventReader eventReader = inputFactory.createXMLEventReader(in);
			// read the XML document
			Product product = null;
			while (eventReader.hasNext()) {
				XMLEvent ev = eventReader.nextEvent();
				if (ev.isStartElement()) {

					switch (ev.asStartElement().getName().getLocalPart()) {
					case PRODUCT:
						// If we have an item element, we create a new item
						product = new Product();
						break;
					case "title":
						ev = eventReader.nextEvent();
					//	System.out.println(ev.asCharacters().getData());
						product.setTitle(ev.asCharacters().getData());
						continue;
					case "toDate":
						ev = eventReader.nextEvent();
						System.out.println(ev.asCharacters().getData());
						product.setToDate(LocalDate.parse(ev.asCharacters().getData(), DateTimeFormatter.ofPattern("dd.MM.yyyy")));
						continue;
					case "fromDate":
						ev = eventReader.nextEvent();
					//	System.out.println(ev.asCharacters().getData());
						product.setFromDate(LocalDate.parse(ev.asCharacters().getData(), DateTimeFormatter.ofPattern("dd.MM.yyyy")));
						continue;
					case "price":
						ev = eventReader.nextEvent();
					//	System.out.println(ev.asCharacters().getData());
						product.setPrice(Float.parseFloat((ev.asCharacters().getData())));
						
						continue;
					}
				}
				
				if (ev.isEndElement()) {
					if (ev.asEndElement().getName().getLocalPart()
							.equals(PRODUCT)) {
						products.add(product);
					}

				}
			}
		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return products;
	}

	public void readSimple(InputStream in) throws IOException {
		// First, create a new XMLInputFactory
		XMLInputFactory inputFactory = XMLInputFactory.newInstance();

		Stack<String> stck = new Stack<String>();

		// Setup a new eventReader
		try {
			XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

			while (eventReader.hasNext()) {
				XMLEvent ev = eventReader.nextEvent();
				if (ev.isStartElement()) {
					stck.push(ev.asStartElement().getName().getLocalPart());

					@SuppressWarnings("unchecked")
					Iterator<Attribute> iter = ev.asStartElement()
							.getAttributes();
					while (iter.hasNext()) {
						Attribute a = iter.next();
						System.out.println(buildXPathString(
								stck,
								"/@" + a.getName().getLocalPart() + "=\""
										+ a.getValue() + "\""));
					}
				}
				if (ev.isCharacters()) {
					String s = ev.asCharacters().getData();
					if (s.trim().length() > 0)
						System.out.println(buildXPathString(stck, "=\"" + s
								+ "\""));
				}
				if (ev.isEndElement())
					stck.pop();

			}

		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private String buildXPathString(Stack<String> stck, String postfix) {
		StringBuffer sb = new StringBuffer();
		for (String s : stck)
			sb.append("/").append(s);
		sb.append(postfix);
		return sb.toString();
	}

}
